﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Exercise1.model;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vehicle> vehicleList = new List<Vehicle>();
            vehicleList.Add(new Plane(1400, "plane", 2012));
            vehicleList.Add(new Car("red", "car", 2018));

            Console.WriteLine(JsonSerializer.Serialize(vehicleList));        }
    }
}