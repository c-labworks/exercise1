﻿﻿namespace Exercise1.model
{
    public class Plane : Vehicle
    {
        public double maxHeight { get; set; }

        public Plane(double maxHeight, string model, int year)
        {
            this.model = model;
            this.year = year;
            this.maxHeight = maxHeight;
        }
    }
}