﻿﻿namespace Exercise1.model
{
    public class Car : Vehicle
    {
        public string colour { get; set; }

        public Car(string colour, string model, int year)
        {
            this.model = model;
            this.year = year;
            this.colour = colour;
        }
    }
    
    
    
}